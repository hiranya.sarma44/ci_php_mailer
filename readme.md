## CodeIgniter Php Mailer Tutorial

* Download and unzip CodeIgniter

* Download https://github.com/PHPMailer/PHPMailer, unzip and save the content of src/ directory to application/third_party/php_mailer/ of CodeIgniter directory.

* Create Mail Controller.

* Above the Mail Controller Class add the following -
```php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require_once APPPATH.'third_party/php_mailer/Exception.php';
require_once APPPATH.'third_party/php_mailer/PHPMailer.php';
require_once APPPATH.'third_party/php_mailer/SMTP.php';
```

* Paste the following code to Mail Controller -
```php
public function index()
	{
		$mail = new PHPMailer(true);

		try {
			//Server settings
			$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
			$mail->isSMTP();                                            // Send using SMTP
			$mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
			$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
			$mail->Username   = 'your@gmail.com';                     // SMTP username
			$mail->Password   = 'your_password';                               // SMTP password
			$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
			$mail->Port       = 2525;// or 587 or 25                                   // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

			//Recipients
			$mail->setFrom('your@gmail.com', 'Mailer');
			$mail->addAddress('recipient@gmail.com', 'Joe User');     // Add a recipient
//			$mail->addAddress('ellen@example.com');               // Name is optional
//			$mail->addReplyTo('info@example.com', 'Information');
//			$mail->addCC('cc@example.com');
//			$mail->addBCC('bcc@example.com');

			// Attachments
//			$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
//			$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

			// Content
			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->Subject = 'Here is the subject';
			$mail->Body    = 'This is the HTML message body <b>in bold!</b>';
			$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

			$mail->send();
			echo "<p style='color:white;background-color: #3fec33'>Message has been sent</p>";
		} catch (Exception $e) {
			echo "<p style='color:white;background-color: #ec334f'>Message could not be sent. Mailer Error: {$mail->ErrorInfo}</p>";
		}
	}
```

* Make the necessary changes and goto localhost/mail. your mail will be sent.

N.B :  The Mail Sender Account should be made less secure. For gmail user there is a `helper.png`.
